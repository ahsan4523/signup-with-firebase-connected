
import 'package:flutter/material.dart';

class SuccessfulScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Success'),
          backgroundColor: Colors.deepOrange[500],toolbarHeight: 40),
      body: Center(
        child: Text('Login Successful!'),
      ),
    );
  }
}

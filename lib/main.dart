import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'package:npp/signin.dart';

Future<void> main() async
{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.amber[50], //<-- SEE HERE
      ),
      debugShowCheckedModeBanner: false,
      home: SignUpScreen(),
    );
  }
}